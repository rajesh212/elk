#INSTALL JAVA
echo "INSTALLING JAVA"
echo " " 
echo " " 
echo " " 
sudo apt update;sudo apt install openjdk-8-jdk -y;echo 'export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"' | sudo tee -a /etc/profile;source /etc/profile;echo $JAVA_HOME;  >/dev/null 2>&1
echo "######################################################################"
echo "######################################################################"
echo "                        JAVA INSTALLED"
echo "######################################################################"
echo "######################################################################"
echo " " 
echo " " 
echo " " 
echo "INSTALLING ELASTICSEARCH"
#INSTALL ELASTICSEARCH
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -;sudo apt install apt-transport-https;echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list;sudo apt update && sudo apt  install elasticsearch -y; > /dev/null 2>&1
sudo sed -i '/node.name/s/^#//g' /etc/elasticsearch/elasticsearch.yml  >/dev/null 2>&1
sudo sed -i '55anetwork.host: 0.0.0.0' /etc/elasticsearch/elasticsearch.yml  >/dev/null 2>&1
sudo sed -i '68adiscovery.seed_hosts: ["127.0.0.1"]' /etc/elasticsearch/elasticsearch.yml  >/dev/null 2>&1
sudo sed -i '72acluster.initial_master_nodes: ["node-1"]' /etc/elasticsearch/elasticsearch.yml  >/dev/null 2>&1
sudo /bin/systemctl daemon-reload  >/dev/null 2>&1
sudo /bin/systemctl enable elasticsearch.service  >/dev/null 2>&1
sudo /bin/systemctl  start elasticsearch.service  >/dev/null 2>&1
curl -XGET 127.0.0.1:9200  >/dev/null 2>&1
echo " " 
echo " " 
echo " " 
echo "######################################################################"
echo "######################################################################"
echo "                        ELASTIC SEARCH INSTALLED"
echo "######################################################################"
echo "######################################################################"
echo " " 
echo " " 
echo " " 
echo "INSTALLING KIBANA"
#INSTALL KIBANA
sudo apt update && sudo apt install kibana -y;
sudo sed -i '5aserver.host: 0.0.0.0' /etc/kibana/kibana.yml >/dev/null 2>&1
sudo sed -i '/elasticsearch.hosts/s/^#//g' /etc/kibana/kibana.yml >/dev/null 2>&1
sudo /bin/systemctl  daemon-reload  >/dev/null 2>&1
sudo /bin/systemctl enable kibana.service  >/dev/null 2>&1
sudo /bin/systemctl  start kibana.service  >/dev/null 2>&1
echo " "
echo " "
echo " "
echo "######################################################################"
echo "######################################################################"
echo "                        KIBANA INSTALLED"
echo "######################################################################"
echo "######################################################################"
echo " "
echo " "
echo " "
#INSTALL LOGSTASH
echo "INSTALLING LOGSTASH"
sudo apt update && sudo apt install logstash -y;
